#!/usr/bin/env bash

# Config parameters
editor=vim
reader=mdv

scriptpath="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"/

add ()
{
	day=$1
	month=$2
	year=$3

	if [ -z ${day} ]
	then
		day="$(date +%-d)"
	fi
	if [ -z ${month} ]
	then
		month="$(date +%-m)"
	fi
	if [ -z ${year} ]
	then
		year="$(date +%Y)"
	fi

	echo Add entry for date: ${day}/${month}/${year}
	read task

	$(mkdir -p ${scriptpath}/${year}/${month}/${day})
	$(touch ${scriptpath}/${year}/${month}/${day}/\[_]_$(echo ${task// /_}))
	echo "# ${task}" > ${scriptpath}/${year}/${month}/${day}/\[_]_$(echo ${task// /_})
	return
}

set_info ()
{
	task=$1

	cd ${scriptpath}

	if [ -z ${task} ]
	then
		echo Enter name of task to edit set info on \(fuzzy, but must be unique\)
		read task
	fi

	task_path=$(find -name "*$(echo ${task// /_})*")
	echo ${task_path}
	mv ${task_path} ${task_path}_+

	${editor} ${task_path}_+

	return
}

get_info ()
{
	task=$1

	cd ${scriptpath}

	if [ -z ${task} ]
	then
		echo Enter name of task to edit get info on \(fuzzy, but must be unique\)
		read task
	fi

	task_path=$(find -name "*${task}*")
	echo ${task_path}

	${reader} ${task_path}

	return
}

tick ()
{
	task=$1

	cd ${scriptpath}

	if [ -z ${task} ]
	then
		echo Enter name of task to tick \(fuzzy, but must be unique\)
		read task
	fi

	task_path=$(find -name "*${task}*")
	updated_task=$(find -name "*${task}*" | sed -r 's/\[_\]/\[x\]/g')

	mv ${task_path} ${updated_task}

	echo "Updated:"
	echo ${updated_task}
	echo ""
	list

	return
}

untick ()
{
	task=$1

	cd ${scriptpath}

	if [ -z ${task} ]
	then
		echo Enter name of task to untick \(fuzzy, but must be unique\)
		read task
	fi

	task_path=$(find -name "*${task}*")
	updated_task=$(find -name "*${task}*" | sed -r 's/\[x\]/\[_\]/g')

	mv ${task_path} ${updated_task}

	echo "Updated:"
	echo ${updated_task}
	echo ""
	list

	return
}

clean_empty_folders ()
{
	cd ${scriptpath}

	rm -d ./*/*/*/ >/dev/null 2>&1
	rm -d ./*/*/ >/dev/null 2>&1
	rm -d ./*/ >/dev/null 2>&1
}

archive ()
{
	task=$1

	cd ${scriptpath}
	mkdir ./.archive >/dev/null 2>&1

	if [ -z ${task} ]
	then
		echo Enter name of task to archive \(fuzzy, but must be unique\)
		read task
	fi

	if [ ${task} == "ticked" ]
	then
		mv ./*/*/*/\[x\]* ./.archive
	else
		task_path=$(find -name "*${task}*")
		echo ${updated_task}
		mv ${task_path} ./.archive
	fi

	clean_empty_folders

	return
}

delete ()
{
	task=$1

	cd ${scriptpath}

	if [ -z ${task} ]
	then
		echo Enter name of task to delete \(fuzzy\)
		read task
	fi

	rm -i ./*/*/*/*$(echo ${task// /_})*
	clean_empty_folders
}

list ()
{
	day=$1
	month=$2
	year=$3

	cd ${scriptpath}

	if [ -z ${month} ]
	then
		month=*
	elif [ ${month} == "a" ]
	then
		month=*
	fi

	if [ -z ${year} ]
	then
		year=20*
	elif [ ${year} == "a" ]
	then
		year=20*
	fi

	if [ -z ${day} ]
	then
		day=*
	elif [ ${day} == "a" ]
	then
		day=*
	elif [ ${day} == "all" ]
	then
		day=*
	elif [ ${day} == "today" ]
	then
		day=$(date +%-d)
		month=$(date +%-m)
		year=$(date +%Y)
	fi

	output=$(ls -1Rc ${year}/${month}/${day})
	echo "Todo's"
	echo ""
	echo ${output} | sed -r 's/[^ ]+/\n&/g' | sed -r 's/(20..)/\n&/g' | tr "_" " "
}

howto ()
{
	echo ""
	echo "--- todo ---"
	echo "simple single script todo system, using filenames as todo entries,"
	echo "the contents of each file is used as the detailed info for each task"
	echo ""
	echo "--- options ---"
	echo "add [day] [month] [year]"
	echo "adds a new task. With no additional parameters, adds a task today"
	echo "optional arguments where the ones not filled in will use todays date"
	echo ""
	echo "set_info [todo name]"
	echo "opens a todo in the preferred text editor for adding additional info, default: vim"
	echo ""
	echo "get_info [todo name]"
	echo "outputs detailed info of task to terminal using preferred reader, default: cat"
	echo ""
	echo "tick [todo name]"
	echo "ticks a todo"
	echo ""
	echo "untick [todo name]"
	echo "unticks todo"task
	echo ""
	echo "archive [todo name]"
	echo "Archives a todo. This implies moving it to the directory .archive"
	echo ""
	echo "archive ticked"
	echo "archives all ticked task"
	echo ""
	echo "delete [todo]"
	echo "deletes a todo"
	echo ""
	echo "list [day] [month] [year]"
	echo "lists todos. Without parameters it lists all. day, month and year can be"
	echo "substituted with "a" to show all. Examle ./todo.sh list 12 a 2015, will show"
	echo "tasks for day 12 of any month in 2015"
	echo ""
	echo "list today"
	echo "lists all todos for today"
	echo ""
	echo "--- configuration ---"
	echo "at the top of the script variables for editor and reader can be set"
	echo ""
}

command=$1
first_arg=$2
second_arg=$3
third_arg=$4
fourth_arg=$5

if [ -z ${command} ]
then
	howto
	exit 1
fi

if [ ${command} == "add" ]
then
	add ${first_arg} ${second_arg} ${third_arg} ${fourth_arg}
elif [ ${command} == "set_info" ]
then
	set_info ${first_arg} ${second_arg} ${third_arg} ${fourth_arg}
elif [ ${command} == "get_info" ]
then
	get_info ${first_arg} ${second_arg} ${third_arg} ${fourth_arg}
elif [ ${command} == "tick" ]
then
	tick ${first_arg} ${second_arg} ${third_arg} ${fourth_arg}
elif [ ${command} == "untick" ]
then
	untick ${first_arg} ${second_arg} ${third_arg} ${fourth_arg}
elif [ ${command} == "archive" ]
then
	archive ${first_arg} ${second_arg} ${third_arg} ${fourth_arg}
elif [ ${command} == "delete" ]
then
	delete ${first_arg} ${second_arg} ${third_arg} ${fourth_arg}
elif [ ${command} == "list" ]
then
	list ${first_arg} ${second_arg} ${third_arg} ${fourth_arg}
else
	howto
fi

