# TODO
Simple single script todo system, using filenames as todo entries,
the contents of each file is used as the detailed info for each task
This is no longer maintained as I found out Vimwiki is a thing

## Prerequesites
The only prerequesite for using this tool out of the box is [mdv](https://github.com/axiros/terminal_markdown_viewer). This is a python based terminal markdown viewer. If you want to avoid this, it is easy to change it in the configuration.

## Options
### add [day] [month] [year]
Adds a new task. With no additional parameters, adds a task today, optional arguments where the ones not filled in will use todays date

### set info [todo name]
Opens a todo in the preferred text editor for adding additional info, default: vim

### get info [todo name]
Outputs detailed info of task to terminal using preferred reader, default: cat

### tick [todo name]
Ticks a todo

### untick [todo name]
Unticks todo"task

### archive [todo name]
Archives a todo. This implies moving it to the directory .archive

### archive ticked
Archives all ticked task

### delete [todo]
Deletes a todo

### list [day] [month] [year]
Lists todos. Without parameters it lists all. day, month and year can be substituted with "a" to show all. Examle ./todo.sh list 12 a 2015, will show tasks for day 12 of any month in 2015

### list today
Lists all todos for today

## Configuration
At the top of the script, variables for editor and reader can be set


